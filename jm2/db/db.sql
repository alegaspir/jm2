CREATE TABLE states (
	id INT NOT NULL AUTO_INCREMENT,
	value VARCHAR(16) NOT NULL,
	
	CONSTRAINT pk_states PRIMARY KEY (id)
);

CREATE TABLE types (
	id INT NOT NULL AUTO_INCREMENT,
	value VARCHAR(16) NOT NULL,
	
	CONSTRAINT pk_types PRIMARY KEY (id)
);

CREATE TABLE builds (
	id INT NOT NULL AUTO_INCREMENT,
	price DECIMAL(10,2) NOT NULL,
	currency VARCHAR(6) NOT NULL,
	description VARCHAR(6000) NOT NULL,
	state_id INT NOT NULL,
	type_id INT NOT NULL,
	ccaa VARCHAR(100) NOT NULL,
	city VARCHAR(100) NOT NULL,
	identifier VARCHAR(100),
	
	CONSTRAINT pk_builds PRIMARY KEY (id),
	CONSTRAINT fk_build_state FOREIGN KEY (state_id) REFERENCES states(id),
	CONSTRAINT fk_build_type FOREIGN KEY (type_id) REFERENCES types(id),
	CONSTRAINT fk_build_type FOREIGN KEY (type) REFERENCES types(id)
);

CREATE TABLE features (
	id INT NOT NULL AUTO_INCREMENT,
	value VARCHAR(100) NOT NULL,
	
	CONSTRAINT pk_features PRIMARY KEY (id)
);

CREATE TABLE build_features (
	id_build INT NOT NULL,
	id_feature INT NOT NULL,
	
	CONSTRAINT pk_build_features PRIMARY KEY (id_build, id_feature),
	CONSTRAINT fk_build_features_build FOREIGN KEY (id_build) REFERENCES builds(id),
	CONSTRAINT fk_build_features_feature FOREIGN KEY (id_feature) REFERENCES features(id)
);

CREATE TABLE equipments (
	id INT NOT NULL AUTO_INCREMENT,
	value VARCHAR(100) NOT NULL,
	
	CONSTRAINT pk_equipments PRIMARY KEY (id)
);

CREATE TABLE build_equipments (
	id_build INT NOT NULL,
	id_equipment INT NOT NULL,
	
	CONSTRAINT pk_build_equipments PRIMARY KEY (id_build, id_equipment),
	CONSTRAINT fk_build_equipments_build FOREIGN KEY (id_build) REFERENCES builds(id),
	CONSTRAINT fk_build_equipments_equipment FOREIGN KEY (id_equipment) REFERENCES equipments(id)
);

CREATE TABLE build_images (
	id INT NOT NULL AUTO_INCREMENT,
	build_id INT NOT NULL,
	path VARCHAR(60) NOT NULL,
	
	CONSTRAINT pk_build_images PRIMARY KEY (id),
	CONSTRAINT fk_build_image FOREIGN KEY (build_id) REFERENCES builds(id)
);

CREATE TABLE users (
	id INTEGER NOT NULL AUTO_INCREMENT,
	username VARCHAR(24) NOT NULL,
	password VARCHAR(60) NOT NULL,
	account_non_expired TINYINT NOT NULL DEFAULT 1,
	account_non_locked TINYINT NOT NULL DEFAULT 1,
	credentials_non_expired TINYINT NOT NULL DEFAULT 1,
	enabled TINYINT NOT NULL DEFAULT 1,
	
	CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE TABLE roles (	
	id INTEGER NOT NULL AUTO_INCREMENT,
	role VARCHAR(12) NOT NULL,
	
	CONSTRAINT pk_roles PRIMARY KEY (id)
);

CREATE TABLE user_roles (
	id_user INTEGER NOT NULL,
	id_role INTEGER NOT NULL,
	
	CONSTRAINT pk_user_roles PRIMARY KEY (id_user, id_role),
	CONSTRAINT fk_user_roles_user FOREIGN KEY (id_user) REFERENCES users(id),
	CONSTRAINT fk_user_roles_role FOREIGN KEY (id_role) REFERENCES roles(id)
);

-- DATA
INSERT INTO states VALUES
(1, 'Alquiler'),
(2, 'Venta'),
(3, 'Transpaso');

INSERT INTO types VALUES
(1, 'Piso'),
(2, 'Chalet');

INSERT INTO `jm2`.`roles`
(`id`,`role`)
VALUES (1,'ROLE_ADMIN');

INSERT INTO `jm2`.`users`
(`id`,`username`,`password`,`account_non_expired`,`account_non_locked`,`credentials_non_expired`,`enabled`)
VALUES (1,'alfonso','123456',1,1,1,1);

INSERT INTO `jm2`.`user_roles`
(`id_user`,`id_role`)
VALUES (1,1);