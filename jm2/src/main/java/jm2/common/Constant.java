package jm2.common;

import java.nio.file.Paths;

public class Constant {

	/**
	 * Status response
	 */
	public final static Integer STATUS_OK = 0;
	public final static Integer STATUS_ERROR = 1;
	
	/**
	 * Identifiers for values
	 */
	public final static String VALUES_STATES = "states";
	public final static String VALUES_TYPES = "types";
	public final static String VALUES_CITIES = "cities";
	
	/**
	 * Pagination
	 */
	public final static Integer DEFAULT_PAGE_SIZE = 5;
	
	/**
	 * Identifiers for result
	 */
	public final static String BUILDS = "builds";
	
	public final static String PAGES = "pages";
	
	/**
	 * Paths images
	 */
	public final static String PATH_TEMP_IMAGES = Paths.get("/").toString() + "jm2-app/images/temp/";
	public final static String PATH_IMAGES = Paths.get("/").toString() + "jm2-app/images/";
}
