package jm2.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = "jm2", excludeFilters = { @Filter(Configuration.class) })
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
public class MainConfig {
	
	@Autowired
	private Environment env;
	
	private final String PACKAGES = "hibernate.packages";
	
	private final String JDBC_NAME = "jdbc.name";
	private final String JDBC_URL = "jdbc.url";
	private final String JDBC_USER = "jdbc.user";
	private final String JDBC_PASSWORD = "jdbc.password";
	
	private final String HIB_FORMAT_SQL = "hibernate.format_sql";
	private final String HIB_SHOW_SQL = "hibernate.show_sql";
	private final String HIB_DIALECT = "hibernate.dialect";
	
	@Bean
	public SessionFactory sessionFactoy() {
		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());
		builder.scanPackages(env.getProperty(PACKAGES));
		builder.addProperties(properties());
		SessionFactory session = builder.buildSessionFactory();
		return session;
	}
	
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty(JDBC_NAME));
		dataSource.setUrl(env.getProperty(JDBC_URL));
		dataSource.setUsername(env.getProperty(JDBC_USER));
		dataSource.setPassword(env.getProperty(JDBC_PASSWORD));
		return dataSource;
	}
	
	@Bean
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactoy());
		return transactionManager;
	}

	@Bean
	public PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	private Properties properties() {
		Properties properties = new Properties();
		properties.put(HIB_FORMAT_SQL, env.getProperty(HIB_FORMAT_SQL));
		properties.put(HIB_SHOW_SQL, env.getProperty(HIB_SHOW_SQL));
		properties.put(HIB_DIALECT, env.getProperty(HIB_DIALECT));
		return properties;
	}

}
