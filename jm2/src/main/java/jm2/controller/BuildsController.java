package jm2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jm2.common.Constant;
import jm2.common.GenericResponse;
import jm2.model.build.Build;
import jm2.service.interfaces.IBuildService;

@RestController
@RequestMapping(value = "/build")
public class BuildsController {

	@Autowired
	private IBuildService buildService;
	
	@RequestMapping(
			value = "/add",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse addBuild(
			@RequestBody Build build) {
		
		GenericResponse response = new GenericResponse();
		
		try {
			buildService.save(build);
			response.setStatus(Constant.STATUS_OK);
		} catch (Exception e) {
			response.setStatus(Constant.STATUS_ERROR);
		}
		
		return response;
	}
	
	@RequestMapping(
			value = "/delete",
			method = RequestMethod.POST)
	public GenericResponse deleteBuild(
			@RequestParam("id") int id) {
		// TODO
		return new GenericResponse();
	}
	
	@RequestMapping(
			value = "/builds",
			// TODO: Esto deberia ser un GET. Mirar como hacer para que lea la peticion GET correctamente.
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse findBuilds(@RequestBody String request) {
		
		GenericResponse response = new GenericResponse();
		
		try {
			response.setResult(buildService.findBuildsBySearch(request));
			response.setStatus(Constant.STATUS_OK);
		} catch (Exception e) {
			response.setStatus(Constant.STATUS_ERROR);
		}
		
		return response;
	}
	
}
