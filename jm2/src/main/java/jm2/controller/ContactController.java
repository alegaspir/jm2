package jm2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jm2.common.Constant;
import jm2.common.GenericResponse;
import jm2.model.BuildContactForm;
import jm2.service.interfaces.IContactService;

@RestController
@RequestMapping(value = "/contact")
public class ContactController {

	@Autowired
	private IContactService contactService;
	
	@RequestMapping(value = "/request",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public GenericResponse request(@RequestBody BuildContactForm form) {
		
		GenericResponse response = new GenericResponse();
		response.setStatus(Constant.STATUS_ERROR);
		
		try {
			if (contactService.sendRequest(form)) {
				response.setStatus(Constant.STATUS_OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
}
