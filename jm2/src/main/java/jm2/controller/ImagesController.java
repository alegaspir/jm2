package jm2.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jm2.common.Constant;
import jm2.common.GenericResponse;
import jm2.service.interfaces.IImagesService;

@Controller
@RequestMapping(value = "/image")
public class ImagesController {

	@Autowired
	private IImagesService imageService;
	
	@RequestMapping(value = "/save")
	@ResponseBody
	public GenericResponse saveImages(HttpServletRequest request, Principal user) {
		
		GenericResponse response = new GenericResponse();
		
		try {
			imageService.saveTempImages(request, user.getName());
			response.setStatus(Constant.STATUS_OK);
		} catch (Exception e) {
			response.setStatus(Constant.STATUS_ERROR);
		}
		
		return response;
	}

	@RequestMapping(value = "/{name:.+}")
	public void getImage(@PathVariable("name") String name, HttpServletResponse response) {
		
		try {
			Path path = Paths.get(Constant.PATH_IMAGES + name);
			String mediaType = MediaType.parseMediaType(Files.probeContentType(path)).toString();
			byte[] content = Files.readAllBytes(path);
			
			response.setContentType(mediaType);
			response.getOutputStream().write(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
