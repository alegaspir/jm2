package jm2.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jm2.common.Constant;
import jm2.common.GenericResponse;
import jm2.service.interfaces.IValuesService;

@Controller
@RequestMapping(value = "/values")
public class ValuesController {

	@Autowired
	private IValuesService valuesService;
	
	@RequestMapping(
			value = "/all",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GenericResponse findValues() {
		
		GenericResponse response = new GenericResponse();
		Map<String, Object> values = new HashMap<String, Object>();
		
		try {
			values.put(Constant.VALUES_STATES, valuesService.findAllStates());
			values.put(Constant.VALUES_TYPES, valuesService.findAllTypes());
			values.put(Constant.VALUES_CITIES, valuesService.findAllCities());
			response.setResult(values);
			response.setStatus(Constant.STATUS_OK);
		} catch (Exception e) {
			response.setStatus(Constant.STATUS_ERROR);
		}
		
		return response;
	}
	
}
