package jm2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

	@RequestMapping(value = { "", "/" })
	public String index() {
		return "index";
	}
	
	@RequestMapping(value = "home")
	public String home() {
		return "pages/home";
	}
	
	@RequestMapping(value = "builds")
	public String inmuebles() {
		return "pages/builds";
	}
	
	@RequestMapping(value = "login")
	public String login() {
		return "pages/login";
	}
	
	@RequestMapping(value = "admin")
	public String admin() {
		return "pages/admin";
	}
	
}
