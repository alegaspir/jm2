package jm2.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jm2.dao.interfaces.IBuildDAO;
import jm2.model.BuildSearchForm;
import jm2.model.build.Build;
import jm2.model.common.Pagination;

@Repository
public class BuildDAOImpl implements IBuildDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void save(Build build) {
		sessionFactory.getCurrentSession().save(build);
	}

	public void delete(Build build) {
		sessionFactory.getCurrentSession().delete(build);
	}

	@SuppressWarnings("unchecked")
	public List<Build> findBuildsBySearch(BuildSearchForm form, Pagination pagination) {
		
		List<Build> result = null;
		Query query = getQuerySearch(form);
		
		query.setFirstResult((pagination.getPageNumber() - 1) * pagination.getPageSize());
		query.setMaxResults(pagination.getPageSize());
		
		result = query.list();
		
		return result;
	}
	
	public Number findBuildsBySearchCount(BuildSearchForm form) {
		
		Number result = null;
		Criteria criteria = getCriteriaSearch(form);
		
		result = (Number) criteria.setProjection(Projections.rowCount()).uniqueResult();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findBuildsCities() {
		
		String hql = "SELECT DISTINCT city FROM Build";
		List<String> cities = sessionFactory.getCurrentSession()
													.createQuery(hql)
												.list();
		return cities;
	}
	
	private Criteria getCriteriaSearch(BuildSearchForm form) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Build.class);
		
		if (form.getType() != null)
			criteria.add(Restrictions.eq("type", form.getType()));
		
		if (form.getCity() != null)
			criteria.add(Restrictions.eq("city", form.getCity()));
		
		if (form.getState() != null)
			criteria.add(Restrictions.eq("state", form.getState()));
		
		if (form.getPriceMin() != null)
			criteria.add(Restrictions.ge("price", form.getPriceMin()));
		
		if (form.getPriceMax() != null)
			criteria.add(Restrictions.le("price", form.getPriceMax()));
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return criteria;
	}
	
	private Query getQuerySearch(BuildSearchForm form) {
		String hql = "FROM Build WHERE "
				+ "(:type IS NULL OR type = :type) "
				+ "AND (:city IS NULL OR city = :city) "
				+ "AND (:state IS NULL OR state = :state) "
				+ "AND (:priceMin IS NULL OR price >= :priceMin) "
				+ "AND (:priceMax IS NULL OR price <= :priceMax)";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		query.setParameter("type", form.getType());
		query.setParameter("city", form.getCity());
		query.setParameter("state", form.getState());
		query.setParameter("priceMin", form.getPriceMin());
		query.setParameter("priceMax", form.getPriceMax());
		
		return query;
	}

}
