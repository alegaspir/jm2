package jm2.dao.impl;

import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jm2.dao.interfaces.IEquipmentDAO;
import jm2.model.build.Equipment;

@Repository
public class EquipmentDAOImpl implements IEquipmentDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void save(Equipment equipment) {
		sessionFactory.getCurrentSession().save(equipment);
	}
	
	public void saveSet(Set<Equipment> equiments) {
		for (Equipment equipment : equiments) {
			save(equipment);
		}
	}

	public Equipment check(Equipment equipment) {
		String hql = "FROM Equipment WHERE value = :value";
		Equipment result = (Equipment) sessionFactory
										.getCurrentSession()
											.createQuery(hql)
												.setParameter("value", equipment.getValue())
											.uniqueResult();
		return result == null ? equipment : result;
	}

}
