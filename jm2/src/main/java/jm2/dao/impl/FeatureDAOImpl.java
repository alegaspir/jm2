package jm2.dao.impl;

import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jm2.dao.interfaces.IFeatureDAO;
import jm2.model.build.Feature;

@Repository
public class FeatureDAOImpl implements IFeatureDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void save(Feature feature) {
		sessionFactory.getCurrentSession().save(feature);
	}
	
	public void saveSet(Set<Feature> features) {
		for (Feature feature : features) {
			save(feature);
		}
	}

	public Feature check(Feature feature) {
		String hql = "FROM Feature WHERE value = :value";
		Feature result = (Feature) sessionFactory
										.getCurrentSession()
											.createQuery(hql)
												.setParameter("value", feature.getValue())
											.uniqueResult();
		return result == null ? feature : result;
	}

}
