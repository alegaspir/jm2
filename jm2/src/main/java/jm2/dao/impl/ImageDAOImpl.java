package jm2.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jm2.dao.interfaces.IImageDAO;
import jm2.model.build.Image;

@Repository
public class ImageDAOImpl implements IImageDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void save(Image image) {
		sessionFactory.getCurrentSession().save(image);
	}

}
