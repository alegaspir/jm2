package jm2.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jm2.dao.interfaces.IStateDAO;
import jm2.model.build.State;

@Repository
public class StateDAOImpl implements IStateDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<State> findAll() {
		
		String hql = "FROM State";
		List<State> states = sessionFactory
								.getCurrentSession()
									.createQuery(hql)
								.list();
		
		return states;
	}
	
}
