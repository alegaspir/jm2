package jm2.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jm2.dao.interfaces.ITypeDAO;
import jm2.model.build.Type;

@Repository
public class TypeDAOImpl implements ITypeDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<Type> findAll() {
		
		String hql = "FROM Type";
		List<Type> types = sessionFactory
							.getCurrentSession()
								.createQuery(hql)
							.list();
		
		return types;
	}

}
