package jm2.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jm2.dao.interfaces.IUserDAO;
import jm2.model.user.User;

@Repository
public class UserDAOImpl implements IUserDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public User findByUsername(String username) {
		
		String hql = "FROM User WHERE username = :username";
		User user = (User) sessionFactory.getCurrentSession()
				.createQuery(hql)
				.setParameter("username", username)
				.uniqueResult();
		
		return user;
	}
	
}
