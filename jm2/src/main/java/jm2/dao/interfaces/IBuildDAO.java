package jm2.dao.interfaces;

import java.util.List;

import jm2.model.BuildSearchForm;
import jm2.model.build.Build;
import jm2.model.common.Pagination;

public interface IBuildDAO {

	public void save(Build build);
	
	public void delete(Build build);
	
	public List<Build> findBuildsBySearch(BuildSearchForm form, Pagination pagination);
	
	public Number findBuildsBySearchCount(BuildSearchForm form);
	
	public List<String> findBuildsCities();
}
