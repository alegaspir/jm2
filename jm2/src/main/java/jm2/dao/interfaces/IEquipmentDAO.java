package jm2.dao.interfaces;

import java.util.Set;

import jm2.model.build.Equipment;

public interface IEquipmentDAO {

	public void save(Equipment equipment);
	
	public void saveSet(Set<Equipment> equiments);
	
	public Equipment check(Equipment equipment);
	
}
