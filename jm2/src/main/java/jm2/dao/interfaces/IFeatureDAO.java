package jm2.dao.interfaces;

import java.util.Set;

import jm2.model.build.Feature;

public interface IFeatureDAO {

	public void save(Feature feature);
	
	public void saveSet(Set<Feature> features);
	
	public Feature check(Feature feature);
	
}
