package jm2.dao.interfaces;

import jm2.model.build.Image;

public interface IImageDAO {

	public void save(Image image);
	
}
