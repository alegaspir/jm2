package jm2.dao.interfaces;

import java.util.List;

import jm2.model.build.State;

public interface IStateDAO {

	public List<State> findAll();
	
}
