package jm2.dao.interfaces;

import java.util.List;

import jm2.model.build.Type;

public interface ITypeDAO {

	public List<Type> findAll();
	
}
