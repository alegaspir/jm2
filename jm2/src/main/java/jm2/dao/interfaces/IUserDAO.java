package jm2.dao.interfaces;

import jm2.model.user.User;

public interface IUserDAO {

	public User findByUsername(String username);
	
}
