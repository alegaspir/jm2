package jm2.model;

import java.io.Serializable;

import jm2.model.build.State;
import jm2.model.build.Type;

public class BuildSearchForm implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Type type;
	private State state;
	private String city;
	private String price;
	private Float priceMin;
	private Float priceMax;
	
	public BuildSearchForm() { }

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Float getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(Float priceMin) {
		this.priceMin = priceMin;
	}

	public Float getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(Float priceMax) {
		this.priceMax = priceMax;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((priceMax == null) ? 0 : priceMax.hashCode());
		result = prime * result + ((priceMin == null) ? 0 : priceMin.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuildSearchForm other = (BuildSearchForm) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (priceMax == null) {
			if (other.priceMax != null)
				return false;
		} else if (!priceMax.equals(other.priceMax))
			return false;
		if (priceMin == null) {
			if (other.priceMin != null)
				return false;
		} else if (!priceMin.equals(other.priceMin))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BuildSearchForm [type=" + type + ", state=" + state + ", city=" + city + ", price=" + price
				+ ", priceMin=" + priceMin + ", priceMax=" + priceMax + "]";
	}
	
}
