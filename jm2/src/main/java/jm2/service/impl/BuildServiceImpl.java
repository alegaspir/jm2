package jm2.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import jm2.common.Constant;
import jm2.dao.interfaces.IBuildDAO;
import jm2.dao.interfaces.IEquipmentDAO;
import jm2.dao.interfaces.IFeatureDAO;
import jm2.model.BuildSearchForm;
import jm2.model.build.Build;
import jm2.model.common.Pagination;
import jm2.service.interfaces.IBuildService;
import jm2.service.interfaces.IImagesService;

@Transactional
@Service
public class BuildServiceImpl implements IBuildService {

	@Autowired
	private IBuildDAO buildDAO;
	
	@Autowired
	private IEquipmentDAO equipmentDAO;
	
	@Autowired
	private IFeatureDAO featureDAO;
	
	@Autowired
	private IImagesService imagesService;
	
	public void save(Build build) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		equipmentDAO.saveSet(build.getEquipments());
		featureDAO.saveSet(build.getFeatures());
		buildDAO.save(build);
		imagesService.saveBuildImages(build, username);
	}

	public void delete(Build build) {
		buildDAO.delete(build);
	}

	public Map<String, Object> findBuildsBySearch(String request) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode node = mapper.readTree(request);
			BuildSearchForm form = mapper.convertValue(node.get("form"), BuildSearchForm.class);
			Pagination pagination = mapper.convertValue(node.get("pagination"), Pagination.class);
			result.put(Constant.BUILDS, buildDAO.findBuildsBySearch(form, pagination));
			result.put(Constant.PAGES, (buildDAO.findBuildsBySearchCount(form).intValue() / Constant.DEFAULT_PAGE_SIZE + 1));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		return result;
	}

}
