package jm2.service.impl;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import jm2.model.BuildContactForm;
import jm2.service.interfaces.IContactService;

@Service
public class ContactServiceImpl implements IContactService {

	@Autowired
	private Environment env;
	
	
	
	// TODO hacerlo con spring
	
	public Boolean sendRequest(BuildContactForm form) {
		Boolean success = Boolean.FALSE;
		
		String to = env.getProperty("mail.contact");
		String from = form.getMail();
		
		try {
			Message message = new MimeMessage(getMailSession());
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,	InternetAddress.parse(to));
			message.setSubject("Solicitud de contacto");
			message.setText("Solicitud de contacto,"
					+ " \n\n Nombre: " + form.getName()
					+ " \n Email: " + form.getMail()
					+ " \n Telefono: " + form.getPhone()
					+ " \n Identificador: " + form.getIdentifier());

			Transport.send(message);
			success = Boolean.TRUE;
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		
		return success;
	}
	
	private Properties getMailProperties() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", env.getProperty("javamail.smtp.auth"));
		props.put("mail.smtp.starttls.enable", env.getProperty("javamail.smtp.starttls.enable"));
		props.put("mail.smtp.host", env.getProperty("javamail.smtp.host"));
		props.put("mail.smtp.port", env.getProperty("javamail.smtp.port"));
		return props;
	}
	
	private Session getMailSession() {
		final String username = env.getProperty("mail.username");
		final String password = env.getProperty("mail.password");
		
		Session session = Session.getInstance(getMailProperties(),
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
		return session;
	}

}
