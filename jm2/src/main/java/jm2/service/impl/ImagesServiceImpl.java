package jm2.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import jm2.common.Constant;
import jm2.dao.interfaces.IImageDAO;
import jm2.model.build.Build;
import jm2.model.build.Image;
import jm2.service.interfaces.IImagesService;

@Transactional
@Service
public class ImagesServiceImpl implements IImagesService {

	@Autowired
	private IImageDAO imageDAO;
	
	public void saveBuildImages(Build build, String username) {
		
		try {
			File tempFolder = new File(Constant.PATH_TEMP_IMAGES + username + "/");
	
			for (File file : tempFolder.listFiles()) {
				Image image = new Image();
				Path from = Paths.get(file.getAbsolutePath());
				Path to = Paths.get(Constant.PATH_IMAGES).resolve(file.getName());
				
				CopyOption[] options = new CopyOption[] {
			      StandardCopyOption.REPLACE_EXISTING,
			      StandardCopyOption.COPY_ATTRIBUTES
			    };
				
				Files.copy(from, to, options);
				image.setPath(file.getName());
				image.setBuild(build);
				imageDAO.save(image);
				file.delete();
			}
			
			tempFolder.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteBuildImages(Build build) {
		for (Image image : build.getImages()) {
			File file = new File(Constant.PATH_IMAGES + image.getPath());
			file.delete();
		}
	}
	
	public void saveTempImages(HttpServletRequest request, String username) {
		
		String tempPath = Constant.PATH_TEMP_IMAGES + username + "/";
		deleteFolder(new File(tempPath));
		
		try {
			MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
			mRequest.getParameterMap();

			Iterator<String> itr = mRequest.getFileNames();
			while (itr.hasNext()) {
				MultipartFile mFile = mRequest.getFile(itr.next());
				File file = new File(tempPath + mFile.getOriginalFilename());
				
				if (file.mkdirs()) {
					file.createNewFile();
					mFile.transferTo(file);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Boolean deleteFolder(File folder) {
		if (folder.listFiles() != null) {
			for (File file : folder.listFiles()) {
				file.delete();
			}
		}
		return folder.delete();
	}

}
