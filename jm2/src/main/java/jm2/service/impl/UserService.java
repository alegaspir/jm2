package jm2.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jm2.dao.interfaces.IUserDAO;
import jm2.model.user.Role;
import jm2.model.user.User;

@Transactional
@Service
public class UserService implements UserDetailsService {

	@Autowired
	private IUserDAO userDAO;
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userDAO.findByUsername(username);
		setAuthorities(user);
		
		return user;
	}
	
	private void setAuthorities(User user) {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (Role role : user.getRoles()) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getRole());
			authorities.add(authority);
		}
		
		user.setAuthorities(authorities);
	}

}
