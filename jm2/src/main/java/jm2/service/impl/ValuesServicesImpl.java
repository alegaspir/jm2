package jm2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jm2.dao.interfaces.IBuildDAO;
import jm2.dao.interfaces.IStateDAO;
import jm2.dao.interfaces.ITypeDAO;
import jm2.model.build.State;
import jm2.model.build.Type;
import jm2.service.interfaces.IValuesService;

@Transactional
@Service
public class ValuesServicesImpl implements IValuesService {

	@Autowired
	private IStateDAO stateDAO;
	
	@Autowired
	private ITypeDAO typeDAO;
	
	@Autowired
	private IBuildDAO buildDAO;
	
	public List<State> findAllStates() {
		return stateDAO.findAll();
	}

	public List<Type> findAllTypes() {
		return typeDAO.findAll();
	}

	public List<String> findAllCities() {
		return buildDAO.findBuildsCities();
	}

}
