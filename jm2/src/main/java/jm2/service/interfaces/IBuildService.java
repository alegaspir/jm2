package jm2.service.interfaces;

import java.util.Map;

import jm2.model.build.Build;

public interface IBuildService {

	public void save(Build build);
	
	public void delete(Build build);
	
	public Map<String, Object> findBuildsBySearch(String request);
	
}
