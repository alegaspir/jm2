package jm2.service.interfaces;

import jm2.model.BuildContactForm;

public interface IContactService {

	public Boolean sendRequest(BuildContactForm form);
	
}
