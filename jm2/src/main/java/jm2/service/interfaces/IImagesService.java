package jm2.service.interfaces;

import javax.servlet.http.HttpServletRequest;

import jm2.model.build.Build;

public interface IImagesService {

	public void saveBuildImages(Build build, String username);
	
	public void deleteBuildImages(Build build);
	
	public void saveTempImages(HttpServletRequest request, String username);
	
}
