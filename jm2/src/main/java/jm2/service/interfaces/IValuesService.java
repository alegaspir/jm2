package jm2.service.interfaces;

import java.util.List;

import jm2.model.build.State;
import jm2.model.build.Type;

public interface IValuesService {

	public List<State> findAllStates();
	
	public List<Type> findAllTypes();
	
	public List<String> findAllCities();
	
}
