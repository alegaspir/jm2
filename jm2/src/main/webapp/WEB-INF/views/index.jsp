<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>

<html>
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1, user-scalable=yes">
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/bootstrap-3.3.5/css/bootstrap.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/app.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/static/js/libs/lightbox2/dist/css/lightbox.css" />
		
		<title>
			<spring:message code="title" text="JM2" />
		</title>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/libs/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/bootstrap-3.3.5/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/libs/angularjs/angular.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/libs/angularjs/angular-route.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/libs/angularjs/angular-animate.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/libs/angularjs/ng-file-upload/ng-file-upload.min.js"></script>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/app.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/controller/homeController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/controller/buildsController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/controller/loginController.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/controller/adminController.js"></script>
		
		<script>
			var staticUrl = '${pageContext.request.contextPath}';
		</script>
	</head>
	
	<body ng-app="app">
		
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				
				<div class="navbar-header">
					<button type="button" 
						class="navbar-toggle collapsed"
						data-toggle="collapse" 
						data-target="#navbar"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#/">
						JM2
					</a>
				</div>

				<div class="collapse navbar-collapse"
					id="navbar">
					<ul class="nav navbar-nav">
						<li role="presentation">
							<a href="#/">
								Inicio
							</a>
						</li>
						<li role="presentation">
							<a href="#builds">
								Inmbuebles
							</a>
						</li>
					</ul>
					
					<ul class="nav navbar-nav navbar-right">
						<li role="presentation">
							<a href="#login">
								Administracion
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		
		<div class="container">
			<div ng-view></div>
		</div>
		
		<div id="modalMessage" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" 
							class="close" 
							data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
						<h4 class="modal-title green">
							Mensaje de error
						</h4>
					</div>
					<div class="modal-body">
						<span id="modalMessageContent"></span>
					</div>
					<div class="modal-footer">
						<button type="button" 
							class="btn btn-default"
							data-dismiss="modal">
							Cerrar
						</button>
					</div>
				</div>
			</div>
		</div>
		
		<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/libs/lightbox2/dist/js/lightbox.min.js"></script>
	</body>

</html>