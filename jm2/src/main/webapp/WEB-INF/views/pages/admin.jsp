<div class="row">
	
	<div class="col-md-6">
		<form ng-submit="submit()">
			
			<div class="input-group">
				<div class="input-group-addon">
					Precio
				</div>
				<div class="col-md-10 noPadding">
					<input type="text"
						class="form-control"
						ng-model="build.price" />
				</div>
				<div class="col-md-2 noPadding">
					<input type="text"
						class="form-control"
						ng-model="build.currency" />
				</div>
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Descripcion
				</div>
				<textarea type="text"
					class="form-control"
					ng-model="build.description">
				</textarea>
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Estado
				</div>
				<select class="form-control"
					ng-init="build.state = states[0]"
					ng-model="build.state"
					ng-options="state.value for state in states">
				</select>
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Tipo
				</div>
				<select class="form-control"
					ng-init="build.type = types[0]"
					ng-model="build.type"
					ng-options="type.value for type in types">
				</select>
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					CCAA
				</div>
				<input type="text"
					class="form-control"
					ng-model="build.ccaa" />
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Ciudad
				</div>
				<input type="text"
					class="form-control"
					ng-model="build.city" />
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Imagenes
				</div>
				<input
					id="buildImages" 
					type="file"
					class="form-control"
					ngf-select="uploadFiles(this);"
					ng-model="files"
					name="files"
					accept="image/*"
					multiple
					required="" />
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Identificador
				</div>
				<input type="text"
					class="form-control"
					ng-model="build.identifier" />
			</div>
			
			<section class="panel panel-default margedPanelTop">
				<header class="panel-heading">
					Caracteristicas
				</header>
				
				<article id="features" 
					class="panel-body">
					<div ng-repeat="feature in build.features">
						<div>{{ feature.value }}</div>
					</div>
				</article>
			</section>
			
			<section class="panel panel-default">
				<header class="panel-heading">
					Equipamiento
				</header>
				
				<article id="equipments" 
					class="panel-body">
					<div ng-repeat="equipment in build.equipments">
						<div>{{ equipment.value }}</div>
					</div>
				</article>
			</section>
			
			<button class="btn btn-success">
				Guardar
			</button>
		
		</form>
	</div>
	
	<div class="col-md-6">
		
		<section class="panel panel-default">
			<header class="panel-heading">
				A�adir caracteristica
			</header>
			
			<article class="panel-body">
				<div class="row">
					<div class="col-md-10">
						<div class="input-group">
							<div class="input-group-addon">
								Caracteristica
							</div>
							<input id="feature" 
								type="text"
								class="form-control" />
						</div>
					</div>
					<div class="col-md-2">
						<button class="btn" 
							ng-click="addFeature();">
							A�adir
						</button>
					</div>
				</div>
			</article>
		</section>
		
		<section class="panel panel-default">
			<header class="panel-heading">
				A�adir equipamiento
			</header>
			
			<article class="panel-body">
				<div class="row">
					<div class="col-md-10">
						<div class="input-group">
							<div class="input-group-addon">
								Equipamiento
							</div>
							<input id="equipment"
								type="text"
								class="form-control" />
						</div>
					</div>
					<div class="col-md-2">
						<button class="btn"
							ng-click="addEquipment();">
							A�adir
						</button>
					</div>
				</div>
			</article>
		</section>
		
	</div>
	
</div>