<div class="row">

	<div class="col-md-4">
		<form ng-submit="search();">
			
			<div class="input-group">
				<div class="input-group-addon">
					Tipo
				</div>
				<select class="form-control"
					ng-model="form.type"
					ng-options="type.value for type in types">
					<option value>Todos</option>
				</select>
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Ciudad
				</div>
				<select class="form-control"
					ng-model="form.city"
					ng-options="city for city in cities">
					<option value>Todas</option>
				</select>
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Estado
				</div>
				<select class="form-control"
					ng-model="form.state"
					ng-options="state.value for state in states">
					<option value>Todos</option>
				</select>
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Precio minimo
				</div>
				<input type="number" 
					class="form-control"
					ng-model="form.priceMin" />
			</div>
			
			<div class="input-group">
				<div class="input-group-addon">
					Precio maximo
				</div>
				<input type="number"
					class="form-control"
					ng-model="form.priceMax" />
			</div>
		
			<input class="btn btn-success right" type="submit">
		
		</form>
	</div>
	
	<section class="col-md-8">
		<div ng-repeat="build in builds">
			<article class="panel panel-default">
				
				<header class="panel-heading">
					{{ build.ccaa }} - {{ build.city }} 
					<span class="right">{{ build.price }} {{ build.currency }}</span>	
				</header>
				
				<div class="panel-body">
					<div>
						Tipo de inmueble: {{ build.type.value }}
					</div>
					
					<div>
						Estado del inmueble: {{ build.state.value }}
					</div>
				
					<div>
						{{ build.description }}
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<section class="panel panel-default">
								<header class="panel-heading">
									Caracteristicas
								</header>
								
								<article class="panel-body">
									<div ng-repeat="feature in build.features">
										<span class="glyphicon glyphicon-tag green"></span>
										{{ feature.value }}
									</div>
								</article>
							</section>
						</div>
						
						<div class="col-md-6">
							<section class="panel panel-default">
								<header class="panel-heading">
									Equipamientos
								</header>
								
								<article class="panel-body">
									<div ng-repeat="equipment in build.equipments">
										<span class="glyphicon glyphicon-triangle-right green"></span>
										{{ equipment.value }}
									</div>
								</article>
							</section>
						</div>
					</div>
					
					<section class="panel panel-default">
						<header class="panel-heading">
							Imagenes
						</header>
						
						<article class="panel-body">
							<div class="">
								<div ng-repeat="image in build.images" class="crop col-md-3">
									<a href="image/{{ image.path }}" data-lightbox="gallery{{ build.id }}">
										<img src="image/{{ image.path }}" />
									</a>
								</div>
							</div>
						</article>
					</section>
					
					<div>
						<button class="btn" data-toggle="modal" data-target="#contact{{ build.id }}">
							Solicitar contacto
						</button>
					</div>
				</div>
				
			</article>

			<div id="contact{{ build.id }}" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" 
								class="close" 
								data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">
									&times;
								</span>
							</button>
							<h4 class="modal-title green">
								Solicitud de contacto
							</h4>
						</div>
						<form id="contactRequest{{ build.id }}" 
							ng-submit="contactRequest(build.id);">
							<div class="modal-body">
								<div class="input-group">
									<div class="input-group-addon">
										Nombre
									</div>
									<input type="text"
										class="form-control"
										name="name" />
								</div>
							
								<div class="input-group">
									<div class="input-group-addon">
										E-mail
									</div>
									<input type="text"
										class="form-control"
										name="mail" />
								</div>
								
								<div class="input-group">
									<div class="input-group-addon">
										Telefono
									</div>
									<input type="text"
										class="form-control"
										name="phone" />
								</div>
								
								<input type="hidden"
									name="identifier"
									value="{{ build.identifier }}" />
							</div>
							<div class="modal-footer">
								<button type="button" 
									class="btn btn-default"
									data-dismiss="modal">
									Cerrar
								</button>
								<input type="submit" 
									class="btn btn-success"
									value="Enviar solicitud" />
							</div>
						</form>
					</div>
				</div>
			</div>
			
		</div>
		
		<nav>
			<ul class="pagination">
				<li ng-repeat="page in pages track by $index"
					ng-class="isActive($index + 1)">
					<a ng-click="search($index + 1);">
						{{ $index + 1 }}
					</a>
				</li>
			</ul>
		</nav>
	</section>

</div>