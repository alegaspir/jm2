<div class="row">
	<div class="col-md-8">
		<section class="panel panel-default">
			<header class="panel-heading">
				Quienes somos
			</header>
			
			<article class="panel-body">
				Somos un grupo de profesionales con una dilatada experiencia inmobiliaria y jur�dica. <br /><br />
				Acompa�amos a nuestros clientes durante todo el proceso de compra-venta, alquiler o traspasos de su casa local oficina, nave, terreno rustico etc, asesor�ndoles y apoy�ndoles en todos los aspectos inmobiliarios y jur�dicos hasta que se concluya la operaci�n. <br /><br />
				Adem�s tambi�n ofrecemos certificaciones energ�ticas sin intermediarios y con precios muy econ�micos.
			</article>
		</section>
	</div>
	
	<div class="col-md-4">
		<section class="panel panel-default">
			<header class="panel-heading">
				Contacto
			</header>
			
			<article class="panel-body">
				Sector Oficios 6 y 7. Tres Cantos (Madrid). <br />
				<span class="labelContact">E-mail:</span> info@jm2inmobiliaria.es <br />
				<span class="labelContact">Telefono fijo:</span> 91 804 26 25 <br />
				<span class="labelContact">Telefono movil:</span> 606 45 44 82 <br />
			</article>
		</section>
	</div>
</div>

<section class="panel panel-default">
	<header class="panel-heading">
		Localizacion
	</header>
	
	<article class="panel-body">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12116.203729056087!2d-3.7142955!3d40.6066999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x67a3622a65cc2cd8!2sJM2+Inmobiliaria!5e0!3m2!1ses!2ses!4v1444503787423" 
			width="100%" 
			height="450" 
			frameborder="0" 
			style="border:0" 
			allowfullscreen>
		</iframe>
	</article>
</section>