<form
	class="loginForm"
	ng-submit="login();">
	
	<div class="input-group">
		<div class="input-group-addon">
			Usuario
		</div>
		<input type="text" 
			class="form-control"
			ng-model="credentials.username">
		</input>
	</div>
	
	<div class="input-group">
		<div class="input-group-addon">
			Contraseņa
		</div>
		<input type="password"
			class="form-control"
			ng-model="credentials.password">
		</input>
	</div>
	
	<input class="right btn btn-success"
		value="Entrar" 
		type="submit">
</form>