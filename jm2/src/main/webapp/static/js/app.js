var STATUS_OK = 0;
var STATUS_ERROR = 1;

var MSG_ERROR_500 = "Ha ocurrido un error, la solicitud no se ha procesado correctamente.";
var MSG_ERROR_CONTACT = "La solicitud de contacto no se ha enviado correctamente, intentelo de nuevo o utilice el contacto que hay en la pagina de inicio.";

var app = angular.module('app', ['ngRoute', 'ngFileUpload']);

app.config(['$routeProvider', function($routeProvider, $httpProvider) {
	
	$routeProvider
		.when('/', {
			templateUrl: staticUrl + '/home',
	        controller: 'homeController'
		})
		.when('/builds', {
			templateUrl: staticUrl + '/builds',
	        controller: 'buildsController'
		})
		.when('/login', {
			templateUrl: staticUrl + '/login',
	        controller: 'loginController'
		})
		.when('/admin', {
			templateUrl: staticUrl + '/admin',
	        controller: 'adminController'
		})
		.otherwise({
			redirectTo: '/'
		});
	
}])
.run(function($rootScope, $http) {
	$http.get('values/all')
		.success(function(response) {
			$rootScope.states = response.result.states;
			$rootScope.types = response.result.types;
			$rootScope.cities = response.result.cities;
		});
});

var utils = {
		
		checkError: function(response) {
			var status = response.data.status;
			if (STATUS_ERROR == status) {
				utils.showModalMessage(MSG_ERROR_500);
				throw "Error en el servidor";
			}
		},
		
		showModalMessage: function(message) {
			$('#modalMessageContent').html(message);
			$('#modalMessage').modal('show');
		}
		
};