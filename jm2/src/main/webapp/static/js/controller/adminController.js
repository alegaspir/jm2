app.controller('adminController', function($scope, $http) {
	
	$scope.build = {};
	$scope.build.features = [];
	$scope.build.equipments = [];
	
	$scope.addFeature = function() {
		var feature = {};
		feature.value = $('#feature').val();
		
		$scope.build.features.push(feature);
		$('#feature').val('');
	};
	
	$scope.addEquipment = function() {
		var equipment = {};
		equipment.value = $('#equipment').val();
		
		$scope.build.equipments.push(equipment);
		$('#equipment').val('');
	};
	
	$scope.submit = function() {		
		$http({
			url: staticUrl + '/build/add',
			data: $scope.build,
			method: 'POST'
		}).then(
			function(response) {
				utils.checkError(response);
				$scope.build = {};
				$scope.build.features = [];
				$scope.build.equipments = [];
				document.getElementById('buildImages').value = '';
			},
			function(response) {
				utils.showErrorModal(MSG_ERROR_500);
			}
		);
	};
	
	$scope.uploadFiles = function(element) {
		
		var formData = new FormData();
		$.each(element.files, function(key, value) {
			formData.append(key, value);
		});
		
		$http({
			url: staticUrl + '/image/save',
			transformRequest: angular.identity,
			headers: { 'Content-Type': undefined },
			data: formData,
			method: 'POST'
		}).then(
			function(response) {
				utils.checkError(response);
			},
			function(response) {
				utils.showErrorModal(MSG_ERROR_500);
			}
		);
	};
	
});