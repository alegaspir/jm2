app.controller('buildsController', function($scope, $http) {
	
	$scope.form = {};
	$scope.activePage = 1;
	
    $scope.search = function(pageNumber) {
    	
    	if (pageNumber == null) {
    		pageNumber = 1;
    	}
    	
    	$scope.activePage = pageNumber;
    	
    	$http({
			url: staticUrl + '/build/builds',
			data: { 
				'form': $scope.form, 
				'pagination': { 
					'pageNumber': pageNumber 
					} 
			},
			method: 'POST'
		}).then(
			function(response) {
				utils.checkError(response);
				$scope.builds = response.data.result.builds;
				$scope.pages = new Array(response.data.result.pages);
			},
			function(response) {
				utils.showErrorModal(MSG_ERROR_500);
			}
		);
    };
    
    $scope.isActive = function(pageNumber) {
    	if ($scope.activePage == pageNumber) {
    		return 'active';
    	}
    }
    
    $scope.contactRequest = function(id) {
    	var formValues = $('#contactRequest' + id + ' :input');
    	var values = {};
    	formValues.each(function() {
            values[this.name] = $(this).val();
        });
    	
    	$http({
			url: staticUrl + '/contact/request',
			data: values,
			method: 'POST'
		}).then(
			function(response) {
				utils.checkError(response);
				$('#contactRequest' + id + ' :input').modal("hide");
			},
			function(response) {
				utils.showErrorModal(MSG_ERROR_CONTACT);
			}
		);
    	
    	console.log(values);
    }
    
    $scope.search(1);
});